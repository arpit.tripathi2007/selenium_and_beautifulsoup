from selenium import webdriver
import subprocess, time
import captcha_crop_code_electrol as ccce
import api_captcha_read  as acr
import os

def electrol_search():
    image_name = 'electrol.png'
    width_const = 1
    height_const = 1
    options = webdriver.ChromeOptions()

    chromedriver = subprocess.getoutput("which chromedriver")
    # options.add_argument('--headless')
    # options.add_argument('--no-sandbox')
    driver = webdriver.Chrome(
        chromedriver,
        options=options)
    driver.set_window_size(1024, 600)
    driver.maximize_window()
    driver.delete_all_cookies()
    print("Working")

    driver.get('https://electoralsearch.in/##resultArea')
    driver.find_element_by_xpath('//*[@id="continue"]').click()
    driver.find_element_by_xpath('//*[@id="mainContent"]/div[2]/div/div/ul/li[2]').click()

    element = driver.find_element_by_xpath('//*[@id="captchaEpicImg"]')
    result_captcha_b64 = ccce.captcha_fetch(element, driver, image_name, width_const, height_const)
    result_captcha = acr.captcha_read(str(result_captcha_b64)[2:-1])
    print(result_captcha)
    driver.find_element_by_id('name').send_keys('lbt1381581')
    # driver.find_element_by_id('txtEpicCaptcha').send_keys(str(result_captcha))
    time.sleep(30)




if __name__ == '__main__':
    electrol_search()
