## This is Selenium Code to Scrap Data

Follow Following Steps 
1) Run 'pip install -r requirements.txt'
2) Run any of the following python file as 'python3 filename.py' 
	- parivahan_selenium_code.py (Parivahan Nigam)
	- electrol_search.py (Election Commission)
	- income_tax_india.py (Income Tax India to)

## Details About this Project

We have two different steps that scrap data for alll three abpve mentioned python file

1) Taking Screenshot and cropping captcha and return the image in Base64 format
2) Taking Base64 Image fetched from step 1 and process through API to fetch letters from the captcha Image
