from selenium import webdriver
import subprocess
import captcha_crop_code as ccc
import api_captcha_read  as acr
import json, time, sys
from bs4 import BeautifulSoup
import requests

def parivahan(dl_no, dob):
    image_name = 'parivahan.png'
    width_const = 0.2
    height_const = 0.125
    options = webdriver.ChromeOptions()
    chromedriver = subprocess.getoutput("which chromedriver")
    options.add_argument('--headless')
    options.add_argument('--no-sandbox')
    try:
        driver = webdriver.Chrome(
            chromedriver,
            options=options)
        driver.set_window_size(1024, 600)
        driver.maximize_window()
        driver.delete_all_cookies()
        print("Working")

        driver.get('https://parivahan.gov.in/rcdlstatus/?pur_cd=101')
        element = driver.find_element_by_xpath(
            '//*[@id="form_rcdl:captchaPnl"]/div/div[2]/div/div[2]/table/tbody/tr/td[1]')
        result_captcha_b64 = ccc.captcha_fetch(element, driver, image_name, width_const, height_const)
        captcha_str = str(result_captcha_b64)[2:-1]
        result = acr.captcha_read(captcha_str)
        result = json.loads(result)

        # driver.find_element_by_id('form_rcdl:tf_dlNO').send_keys('Up8720140002671')
        driver.find_element_by_id('form_rcdl:tf_dlNO').send_keys(dl_no)
        # driver.find_element_by_id('form_rcdl:tf_dob_input').send_keys('06-07-1989')
        driver.find_element_by_id('form_rcdl:tf_dob_input').send_keys(dob)
        driver.find_element_by_id('form_rcdl:j_idt33:CaptchaID').send_keys(str(result['captcha']))
        button = driver.find_element_by_id('form_rcdl:j_idt45')
        driver.execute_script("arguments[0].click();", button)
        result_dict = {
            'status': '0',
            'message': ''
        }
        time.sleep(5)
        try:
            text_dl = driver.find_element_by_xpath('//*[@id="form_rcdl:j_idt17"]/div')
            text_dl_status = text_dl.text.split('\n')
            result_dict['status'] = '0'
            result_dict['message'] = text_dl_status[0].split('.')[0]
            print(text_dl.text)
        except Exception as e:
            print(str(e))
            try:
                text_dl = driver.find_element_by_xpath('//*[@id="form_rcdl:j_idt117"]')
                text_dl_status = text_dl.text.split('\n')
                data_dict = {}
                data_inner = {}

                for data in text_dl_status[:5]:
                    data_array = data.split(':')
                    data_dict['_'.join(((data_array[0])).split())] = data_array[1]
                data_inner['_'.join((((text_dl_status[0]).split(':'))[0]).split())] = data_dict

                data_dict = {}
                array_details = text_dl_status[6].split()
                data_dict_inner = {}
                data_dict_inner[array_details[1][:-1]] = array_details[2]
                data_dict_inner[array_details[3][:-1]] = array_details[4]
                data_dict['_'.join((array_details[0]).split('-'))] = data_dict_inner

                array_details = text_dl_status[8].split(':')
                data_dict[('_'.join((array_details[0]).split()))] = (array_details[1].split())[0]
                data_dict[('_'.join((array_details[1].split())[1:]))] = array_details[2]
                data_inner['_'.join((((text_dl_status[5]).split(':'))[0]).split())] = data_dict

                # ['Details Of Driving License: UP8720140002671', 'Current Status: ACTIVE', "Holder's Name: SUNEEL KUMAR", 'Date Of Issue: 09-Jun-2014', 'Last Transaction At: ASST.RTO, KASGANJ',
                # 'Driving License Validity Details', 'Non-Transport From: 09-Jun-2014 To: 08-Jun-2034', 'Transport From: NA To: NA', 'Hazardous Valid Till: NA Hill Valid Till: NA',
                # 'Class Of Vehicle Details', 'COV Category Class Of Vehicle COV Issue Date', 'NT MCWG 09-Jun-2014', 'NT LMV 09-Jun-2014']
                data_dict = []
                array_details_keys = text_dl_status[10].split()
                array_details_values_1 = text_dl_status[11].split()
                array_details_values_2 = text_dl_status[12].split()
                data_inner_cov = {}
                data_inner_cov['_'.join(array_details_keys[0:2])] = (array_details_values_1[0])
                data_inner_cov['_'.join(array_details_keys[2:5])] = (array_details_values_1[1])
                data_inner_cov['_'.join(array_details_keys[5:8])] = (array_details_values_1[2])

                data_dict.append(data_inner_cov)

                data_inner_cov['_'.join(array_details_keys[0:2])] = (array_details_values_2[0])
                data_inner_cov['_'.join(array_details_keys[2:5])] = (array_details_values_2[1])
                data_inner_cov['_'.join(array_details_keys[5:8])] = (array_details_values_2[2])

                data_dict.append(data_inner_cov)

                data_inner['_'.join((((text_dl_status[9]).split(':'))[0]).split())] = data_dict

                result_dict['status'] = '1'
                result_dict['message'] = data_inner
            except Exception as e:
                print(str(e))
                result_dict['status'] = '1'
                result_dict['message'] = 'No DL Found'

    except Exception as e:
        print(str(e))
        result_dict = {
                'status': '0',
                'message': 'Error Occured'
            }
    time.sleep(10)
    return result_dict

if __name__ == '__main__':
    dl_no = sys.argv[1]
    dob = sys.argv[2]
    status = 0
    while (status == 0):
        res = parivahan(dl_no, dob)
        status = res['status']
    print(res)

    # result_count = {'1': 0,
    #                 '0': 0}

    # start_time = time.time()
    # for i in range(100):
    #     print(i+1)
    #     res = parivahan(dl_no, dob)
    #     print(res)
    #     result_count[res['status']] += 1
    #     if (i % 10 == 9):
    #         print(result_count)
    #         new_days = open('result_dl.txt', 'w')
    #         new_days.write(str(result_count))
    # list_time = list()
    # for i in range(10):
    #     print("loop ",i+1)
    #     result_time = dict()
    #     status = 0
    #     count = 0
    #     while (status == 0):
    #         res = parivahan(dl_no, dob)
    #         status = res['status']
    #         count +=1
    #         print("count ",count)
    #     result_dict = json.dumps(res)
    #     time_taken = (int((int(time.time())) - int(start_time)) / 60)
    #     print("--- %s minutes ---" , time_taken)
    #     print(result_dict)
    #     result_time = {'loop': i,
    #                    'time': time_taken,
    #                    'count': count
    #                    }
    #     print(result_time)
    #     list_time.append(result_time)

    # print(list_time)
    # new_days = open('result_dl_time.txt', 'w')
    # new_days.write(str(list_time))
