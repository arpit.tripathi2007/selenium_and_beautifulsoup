from wand.image import Image
from base64 import b64decode, b64encode
import math
from io import BytesIO

def captcha_fetch(element, driver, image_name, width_const, height_const):
    location = element.location;
    size = element.size;
    driver.save_screenshot('parivahan_shot.png')
    img = driver.get_screenshot_as_base64();
    scr_png = b64decode(img)

    x = location['x'];
    y = location['y'];

    width = (location['x'] + size['width']) * width_const;
    height = (location['y'] + size['height']) * height_const;

    im = Image(blob=scr_png)
    # im = im.crop((int(x), int(y), int(width), int(height)))
    # im.save('dl_crop.png')
    im.crop(
        left=math.floor(x),
        top=math.floor(y),
        width=math.ceil(width),
        height=math.ceil(height),
    )
    im.save(filename='parivahan')
    buffered = BytesIO()
    im.save(buffered)
    img_res_b64 = b64encode(buffered.getvalue())

    return img_res_b64
    # print(b64encode(im))