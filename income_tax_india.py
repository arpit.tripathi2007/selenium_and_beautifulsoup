from selenium import webdriver
import subprocess, time, sys
import captcha_crop_code as ccc
import api_captcha_read  as acr
import json

# string_res_base64 = '\n \n'

def income_taxt_india(pan, full_name, dob):
    image_name = 'income.png'
    width_const = 0.26
    height_const = 0.12
    response = {
        'status' : '0',
        'message' : 'Error Occured'
    }
    try:
        options = webdriver.ChromeOptions()
        chromedriver = subprocess.getoutput("which chromedriver")
        options.add_argument('--headless')
        options.add_argument('--no-sandbox')
        driver = webdriver.Chrome(
            chromedriver,
            options=options)
        driver.set_window_size(1024, 600)
        driver.maximize_window()
        driver.delete_all_cookies()
        driver.get('https://www1.incometaxindiaefiling.gov.in/e-FilingGS/Services/VerifyYourPanDeatils.html')
        element = driver.find_element_by_id('captchaImg')
        result_captcha_b64 = ccc.captcha_fetch(element, driver, image_name, width_const, height_const)

        captcha_str = str(result_captcha_b64)[2:-1]

        # global string_res_base64
        # string_res_base64 += captcha_str+'\n \n \n \n'

        result = json.loads(acr.captcha_read(captcha_str))
        driver.find_element_by_id('VerifyYourPanGSAuthentication_pan').send_keys(pan)
        driver.find_element_by_id('VerifyYourPanGSAuthentication_fullName').send_keys(full_name)
        driver.find_element_by_id('dateField').send_keys(dob)
        # print(str(result['captchasolutions']).upper())

        driver.find_element_by_id('VerifyYourPanGSAuthentication_captchaCode').send_keys(
            str(result['captcha']).upper())
        button = driver.find_element_by_id('submitbtn')
        driver.execute_script("arguments[0].click();", button)
        text_status = driver.find_element_by_xpath('//*[@id="staticContentsUrl"]').text
        string_status = text_status.split('\n')
        print(string_status[1])
        pattern = ['PAN is Active and the details are matching with PAN database.', 'PAN is Active but the details are not matching with PAN database.', 'No record found for the given PAN.', 'PAN is Inactive and the details are matching with PAN database.']
        if(string_status[1] not in pattern):
            response['status']  = 0
        else:
            response['status'] = 1
            response['message'] = string_status[1]
        return response
        # PAN is Active but the details are not matching with PAN database.
        # No record found for the given PAN.

    except:
        response['status'] = 0
        return response


if __name__ == '__main__':
    # pan = 'FKPPS9027F'
    # full_name = 'AVINASH SINGH'
    # dob = '17/07/1995'
    pan = sys.argv[1]
    full_name = sys.argv[2]
    dob = sys.argv[3]
    result_count = {1: 0,
                     0 : 0}
    for i in range(100):
        res = income_taxt_india(pan, full_name, dob)
        result_count[res['status']] += 1
        if(i % 10 == 9):
            print(result_count)
            new_days = open('result.txt', 'w')
            new_days.write(str(result_count))

    print(json.dumps(res))
    # new_days.write(string_res_base64)