from PIL import Image
import base64
import os

def captcha_fetch(element, driver, image_name, width_const, height_const):
    location = element.location;
    size = element.size;

    # img = driver.get_screenshot_as_base64()
    png = driver.get_screenshot_as_png()
    driver.save_screenshot('electrol_shot.png')
    x = location['x'];
    y = location['y'];

    width = (location['x'] + size['width']) * width_const;
    height = (location['y'] + size['height']) * height_const;

    x = location['x'];
    y = location['y'];
    width = location['x'] + size['width'];
    height = location['y'] + size['height'];

    im = Image.open('electrol_shot.png')
    im = im.crop((int(x), int(y), int(width)+110, int(height)+50))
    im.save('crop_electrol.png')
    # os.system("convert crop_electrol.png -resize 154x80 crop_electrol.png")

    ## Convert Image to Base64
    b64img = ""
    with open("crop_electrol.png", "rb") as imageFile:
        b64img = base64.b64encode(imageFile.read())



    return b64img
    # print(b64encode(im))